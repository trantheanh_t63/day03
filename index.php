<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="style.css">
</head>

<?php

$gender = array(
    "0" => "Nam",
    "1" => "Nữ"
);

$faculties = array(
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
);

?>

<body>
    <div class="login-body">
        <div class='form'>
            <form>
                <div class="form-item">
                    <div for="name-input" class="label">
                        Họ và tên
                    </div>
                    <input id="name-input" type='text' />
                </div>

                <div class="form-item">
                    <div class="label">
                        Giới tính
                    </div>

                    <?php
                    for ($i = 0; $i < count($gender); $i++) {
                        echo "<div class='radio' ke>
                                <input type='radio' value='$i' />
                                <label> $gender[$i] </label>
                              </div>";
                    }
                    ?>
                </div>

                <div class="form-item">
                    <div class="label">
                        Phân khoa
                    </div>
                    <select>
                        <option value=""></option>
                        <?php
                        foreach ($faculties as $key => $value) {
                            echo "<option value='$key'>$value</option>";
                        }
                        ?>
                    </select>
                </div>

                <div class="center">
                    <button type='submit'>Đăng ký</button>
                </div>
            </form>
        </div>
    </div>
</body>

</html>